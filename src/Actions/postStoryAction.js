import { postType } from "./Type/postType";
import { storyApi } from "../Services/storyApi";
export const postStoryAction = (id) => {
  return async (dispatch) => {
    const detail = await storyApi(id);
    dispatch({ type: postType, payload: detail.data.hits });
  };
};
