import axios from "axios";

export const storyApi = (id) => {
  return axios.get(`${process.env.REACT_APP_API_URL}${id}`);
};
