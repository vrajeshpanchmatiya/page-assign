import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PostStory from "./Components/PostStory";
import Original from "./Components/Original";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={PostStory} />
          <Route path="/Original" component={Original} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
