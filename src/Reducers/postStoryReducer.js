import { postType } from "../Actions/Type/postType";
// Initial Value of Store
const initialState = {
  data: [],
};
// Reducer for Store
export const postStoryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case postType:
      return {
        ...state,
        data: state.data.concat(payload),
      };
    default:
      return state;
  }
};
