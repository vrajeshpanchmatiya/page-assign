import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import { Link } from "react-router-dom";
import { postStoryAction } from "../Actions/postStoryAction";
const PostStory = () => {
  const [id, setId] = useState(0);
  const dispatch = useDispatch();
  // useSelector for fetching the data
  const detail = useSelector((state) => {
    return state.data;
  });
  //useEffect for dispatch id every 10 Seconds
  useEffect(() => {
    setTimeout(() => {
      dispatch(postStoryAction(id));
      setId(id + 1);
    }, 10000);
  }, [dispatch, id]);
  // Columns Field Name
  const columns = [
    {
      Header: "Title",
      accessor: "title",
    },
    {
      Header: "URL",
      accessor: "url",
      filterable: false,
    },
    {
      Header: "CREATED_AT",
      accessor: "created_at",
    },
    {
      Header: "AUTHOR",
      accessor: "author",
      filterable: false,
    },
    {
      Header: "Action",
      Cell: (props) => {
        return (
          <Link to={{ pathname: "/Original", data: props.original }}>
            <Button type="submit">Fetch Row</Button>
          </Link>
        );
      },
    },
  ];
  // React Table for table
  return (
    <ReactTable
      columns={columns}
      data={detail}
      noDataText="Please Wait for a Second"
      defaultPazeSize={20}
      showPageSizeOptions={false}
      filterable
    />
  );
};
export default PostStory;
