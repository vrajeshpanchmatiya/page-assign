import React from "react";
const Original = (props) => {
  // Fetching data from props
  const info = props.location.data;
  // Json Format row
  return <div>{JSON.stringify(info)}</div>;
};
export default Original;
